package com.zoranpavlovic.weather.currentweather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.zoranpavlovic.weather.App;
import com.zoranpavlovic.weather.R;
import com.zoranpavlovic.weather.currentweather.models.WeatherData;

import javax.inject.Inject;

public class CurrentWeatherActivity extends AppCompatActivity implements WeatherView {

    public static final String TAG = "CurrentWeatherActivity";

    @Inject
    WeatherPresenterImpl weatherPresenterImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaggerWeatherComponent.builder()
                .netComponent(((App) getApplicationContext()).getNetComponent())
                .weatherModule(new WeatherModule(this))
                .build().inject(this);

        weatherPresenterImpl.getWeather("London");
    }

    @Override
    public void showWeather(WeatherData weatherData) {
        Log.d(TAG, weatherData.getWeather().get(0).getDescription());
    }

    @Override
    public void showError(String error) {
        Log.d(TAG, error);
    }
}
