package com.zoranpavlovic.weather.currentweather;

import com.zoranpavlovic.weather.currentweather.models.WeatherData;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Zoran on 27/12/2016.
 */

public interface WeatherService {

    @GET("weather?")
    Observable<WeatherData> getWeatherData(@Query("q") String city, @Query("appid") String appId);
}
