package com.zoranpavlovic.weather.currentweather;

import com.zoranpavlovic.weather.common.NetComponent;
import com.zoranpavlovic.weather.common.CustomScope;

import dagger.Component;

/**
 * Created by Zoran on 27/12/2016.
 */

@CustomScope
@Component(dependencies = NetComponent.class, modules = WeatherModule.class)
public interface  WeatherComponent {
    void inject(CurrentWeatherActivity activity);
}
