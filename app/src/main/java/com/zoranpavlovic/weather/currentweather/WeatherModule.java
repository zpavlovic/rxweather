package com.zoranpavlovic.weather.currentweather;

import com.zoranpavlovic.weather.currentweather.WeatherView;
import com.zoranpavlovic.weather.common.CustomScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Zoran on 27/12/2016.
 */
@Module
public class WeatherModule {

    private WeatherView weatherView;

    public WeatherModule(WeatherView weatherView){
        this.weatherView = weatherView;
    }

    @Provides
    @CustomScope
    WeatherView provideWeatherView(){
        return weatherView;
    }



}
