package com.zoranpavlovic.weather.currentweather;

import com.zoranpavlovic.weather.currentweather.models.WeatherData;

/**
 * Created by Zoran on 27/12/2016.
 */

public interface WeatherView {

    void showWeather(WeatherData weatherData);

    void showError(String error);
}
