package com.zoranpavlovic.weather.currentweather;

/**
 * Created by Zoran on 27/12/2016.
 */

public interface WeatherPresenter {

    void getWeather(String city);
}
