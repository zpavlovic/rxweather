package com.zoranpavlovic.weather.currentweather;

import com.zoranpavlovic.weather.currentweather.models.WeatherData;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Zoran on 27/12/2016.
 */

public class WeatherPresenterImpl implements WeatherPresenter {

    Retrofit retrofit;
    WeatherView weatherView;

    @Inject
    public WeatherPresenterImpl(Retrofit retrofit, WeatherView weatherView){
        this.retrofit = retrofit;
        this.weatherView = weatherView;
    }

    @Override
    public void getWeather(String city) {
        WeatherService weatherService = retrofit.create(WeatherService.class);
        weatherService.getWeatherData(city, "fe4b6fea3651349a6c717287c181085b")
        .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<WeatherData>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        weatherView.showError(e.getLocalizedMessage());
                    }

                    @Override
                    public void onNext(WeatherData weatherData) {
                        weatherView.showWeather(weatherData);
                    }
                });
    }
}
