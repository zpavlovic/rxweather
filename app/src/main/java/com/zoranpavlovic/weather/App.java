package com.zoranpavlovic.weather;

import android.app.Application;

import com.zoranpavlovic.weather.common.AppModule;
import com.zoranpavlovic.weather.common.DaggerNetComponent;
import com.zoranpavlovic.weather.common.NetComponent;
import com.zoranpavlovic.weather.common.NetModule;

/**
 * Created by Zoran on 27/12/2016.
 */

public class App extends Application {

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("http://api.openweathermap.org/data/2.5/"))
                .build();    }

    public NetComponent getNetComponent() {
        return netComponent;
    }
}
